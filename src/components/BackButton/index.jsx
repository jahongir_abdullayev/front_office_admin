import { useNavigate } from "react-router-dom";
import "./style.scss";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";

const BackButton = ({ link }) => {
  const navigate = useNavigate();

  return (
    <button type="button" className="BackButton" onClick={() => navigate(link)}>
      <ArrowBackIosIcon className="icon" />
    </button>
  );
};

export default BackButton;
