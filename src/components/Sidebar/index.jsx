import styles from "./style.module.scss";
import { Tooltip } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { NavLink, useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { authActions } from "../../store/auth/auth.slice";
import LogoutIcon from "@mui/icons-material/Logout";
import IconGenerator from "../IconPicker/IconGenerator";
import logo from "/tatu.jpg";
import { elements } from "../../hooks/useSidebarElements/elements";
import { ArrowLeftIcon, ArrowRighticon } from "../../utils/icons";

const Sidebar = () => {
  const location = useLocation();
  const dispatch = useDispatch();

  // const { elements } = useSidebarElements()
  const [rightBlockVisible, setRightBlockVisible] = useState(true);
  const selectedMenuItem = useMemo(() => {
    const activeElement = elements.find((el) => {
      if (location.pathname.includes(el.path)) return true;
      return el.children?.some((child) =>
        location.pathname.includes(child.path)
      );
    });

    return activeElement;
  }, [location.pathname]);

  const logout = () => {
    let body = {
      isAuth: false,
      uid: "",
      email: "",
      emailVerified: true,
      isAnonymous: false,
      providerData: [
        {
          providerId: "",
          uid: "",
          displayName: null,
          email: "",
          phoneNumber: null,
          photoURL: null,
        },
      ],
      stsTokenManager: {
        refreshToken: "",
        accessToken: "",
        expirationTime: null,
      },
      createdAt: "",
      lastLoginAt: "",
      apiKey: "",
      appName: "",
    };
    localStorage.setItem("userInfo", JSON.stringify(body));
    // dispatch(authActions.logout());
  };

  useEffect(() => {
    if (selectedMenuItem?.children) setRightBlockVisible(true);
  }, [selectedMenuItem]);

  return (
    <div
      className={styles.sidebar}
      style={{
        width: rightBlockVisible ? "271px" : "69px",
        overflowX: !rightBlockVisible && "hidden",
        transition: "all 0.3s",
      }}
    >
      <div style={{ width: "100%" }}>
        <div
          className={styles.header}
          onClick={() => setRightBlockVisible((prev) => !prev)}
        >
          <img
            className={styles.logo}
            src={logo}
            alt="tatu-logo"
            style={{
              width: "45px",
              height: "45px",
              borderRadius: "50%",
              objectFit: "cover",
            }}
          />
          <div
            className={styles.title}
            style={{ display: !rightBlockVisible && "none" }}
          >
            TATU
          </div>
        </div>
        <div
          className={styles.arrowIcon}
          onClick={() => setRightBlockVisible((prev) => !prev)}
        >
          {rightBlockVisible ? <ArrowLeftIcon /> : <ArrowRighticon />}
        </div>

        <div
          className={styles.leftSide}
          style={{ width: rightBlockVisible ? "100%" : "69px" }}
        >
          <div
            className={styles.menuItemsBlock}
            style={{ paddingRight: rightBlockVisible && "0px" }}
          >
            {elements.map((element) => (
              <Tooltip
                placement="right"
                followCursor
                key={element.id}
                title={element.title}
              >
                <NavLink
                  key={element.id}
                  // style={{ borderRadius: rightBlockVisible ? "" : "8px" }}
                  to={element.path ?? element.children?.[0]?.path}
                  className={`${styles.menuItem} ${
                    selectedMenuItem?.id === element.id ? styles.active : ""
                  }`}
                >
                  <div className={styles.item}>
                    {typeof element.icon === "string" ? (
                      <IconGenerator icon={element.icon} />
                    ) : (
                      <element.icon
                        color={
                          selectedMenuItem?.id === element.id
                            ? "#012145"
                            : "white"
                        }
                      />
                    )}
                    {rightBlockVisible && (
                      <div
                        className={styles.text}
                        style={{
                          color:
                            selectedMenuItem?.id === element.id
                              ? "#012145"
                              : "",
                        }}
                      >
                        {element.title}
                      </div>
                    )}
                  </div>
                </NavLink>
              </Tooltip>

              // </div>
            ))}
          </div>

          <div className={styles.footer}>
            {/* <div className={styles.menuItem}>
            <NotificationsIcon />
          </div>

          <div className={styles.menuItem}>
            <SettingsIcon />
          </div>

          <UserAvatar disableTooltip /> */}
            {rightBlockVisible && (
              <div className={styles.footer_text}>
                “Toshkent axborot texnologiyalari universiteti” ATDT kafedrasi
                tomonidan tomonidan ko’rsatilgan xizmatlar © 2023
              </div>
            )}

            <div className={styles.menuItem} onClick={logout}>
              <LogoutIcon style={{ color: "white" }} />
            </div>
          </div>
        </div>
      </div>

      {/* <Collapse
        // in={rightBlockVisible}
        orientation="horizontal"
        unmountOnExit
      >
        <div className={styles.rightSide}>
          <div className={styles.header}>
            <Typography className={styles.title} variant="h4">
              {selectedMenuItem?.title}
            </Typography>
            <div
              className={styles.closeButton}
              onClick={() => setRightBlockVisible(false)}
            >
              <KeyboardDoubleArrowLeftIcon />
            </div>
          </div>

          <div className={styles.menuItemsBlock}>
            {elements?.map((childMenuItem) => (
              <NavLink
                to={childMenuItem.path}
                key={childMenuItem.key}
                className={({ isActive }) =>
                  `${styles.menuItem} ${isActive ? styles.active : ""}`
                }
              >
                {childMenuItem.title}
              </NavLink>
            ))}
          </div>
        </div>
      </Collapse> */}
    </div>
  );
};

export default Sidebar;
