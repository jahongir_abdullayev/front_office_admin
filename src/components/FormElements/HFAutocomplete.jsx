import { Autocomplete } from "@mui/material";
import { useEffect, useState } from "react";
import { Controller } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import billingRequest from "../../services/billingService";
import clientTypeService from "../../services/clientTypeService";
import { clientActions } from "../../store/client/client.slice";
import { PHIS_ID, YaTT_ID, YURIDIK_ID } from "../../utils/constant";
import GreenButton from "../Buttons/GreenButton";
import ModalCard from "../ModalCard";
import HFTextField from "./HFTextField";
let debounce = setTimeout(() => {}, 0);

const HFAutocomplete = ({
  control,
  name,
  watch,
  setCheckUser = () => {},
  userType,
  setTin,
  placeholder,
  withName = false,
}) => {
  const phone = watch("phoneNumber");
  const [options, setOptions] = useState([]);
  const [loader, setLoader] = useState(false);
  const [phoneModal, setPhoneModal] = useState(false);
  const [typedTin, setTypedTin] = useState();
  const [clientInfo, setClientInfo] = useState();
  const [loaderForOptionStatus, setLoaderForOptionStatus] = useState(false);
  const dispatch = useDispatch();
  const clientTin = useSelector((state) => state?.client?.tin);
  const [successModal, setSuccesModal] = useState(false);

  const getBillingClients = (searchText) => {
    const params = {
      tin: withName ? "" : searchText,
      search: withName ? searchText : "",
    };
    if (searchText !== "" && searchText) {
      setLoader(true);
      billingRequest
        .getBillingClient(params)
        .then((res) => {
          if (withName) {
            setOptions(
              res?.clients?.map((item) => ({
                label: item?.name,
                value: item?.id,
              }))
            );
          } else {
            setOptions(
              res?.clients?.map((item) => ({
                label: item?.tin,
                value: item?.id,
              }))
            );
          }

          if (res?.clients?.length > 0) {
            setCheckUser(true);
          } else {
            setCheckUser(false);
            if (searchText?.length === 9 || searchText?.length === 14) {
              setLoaderForOptionStatus(true);
              clientTypeService
                .getClientFromSoliq({ tinOrPinfl: searchText })
                .then((res) => setClientInfo({ name: res?.name }));
            }
          }
        })
        .finally(() => setLoader(false));
    }
  };
  const createUser = () => {
    const data = {
      phone,
      tin: typedTin,
      client_type_id:
        userType === "yur"
          ? YURIDIK_ID
          : userType === "fiz"
          ? PHIS_ID
          : YaTT_ID,
      name: clientInfo?.name,
    };

    billingRequest.createBillingClient(data).then((res) => {
      dispatch(clientActions.setClientTin(typedTin));
      setPhoneModal(false);
      setSuccesModal(true);
    });
  };
  const onSearch = (e) => {
    setTypedTin(e?.target?.value);
    clearTimeout(debounce);
    debounce = setTimeout(() => {
      getBillingClients(e?.target?.value);
    }, 700);
  };

  useEffect(() => {
    if (clientTin) {
      setTin(clientTin);
      setCheckUser(true);
    }
  }, [clientTin]);

  return (
    <>
      <Controller
        render={({ onChange, ...props }) => (
          <Autocomplete
            defaultValue={clientTin || ""}
            popupIcon={""}
            onInputChange={onSearch}
            filterOptions={(option) => option}
            noOptionsText={
              loader ? (
                "Qidirilmoqda..."
              ) : (typedTin?.length === 9 || typedTin?.length === 14) &&
                loaderForOptionStatus ? (
                <div
                  onClick={() => setPhoneModal(true)}
                  style={{ cursor: "pointer" }}
                >
                  STIR bazada mavjud emas, mijozni bazaga qo'shishni
                  xohlaysizmi?
                </div>
              ) : (
                "Ro'yxat bo'sh"
              )
            }
            onChange={(e) => {
              setTin(e?.target?.innerText);
            }}
            options={options || []}
            renderInput={(params) => (
              <HFTextField
                {...params}
                control={control}
                name={name}
                placeholder={
                  withName
                    ? "Ism familiyani kiriting"
                    : placeholder
                    ? placeholder
                    : userType === "yur"
                    ? "INNni kiriting*"
                    : "INN yoki PINFLni kiriting*"
                }
              />
            )}
            {...props}
          />
        )}
        name={name}
        control={control}
      />

      <ModalCard open={phoneModal} onClose={setPhoneModal}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            height: "100%",
            textAlign: "center",
            justifyContent: "center",
            gap: "15px",
            flexDirection: "column",
          }}
        >
          <div> Telefon raqamni kiriting </div>
          <HFTextField control={control} name="phoneNumber" width="300px" />
          <GreenButton
            title="Qo'shish"
            style={{ background: "#325ecd" }}
            onClick={() => createUser()}
          />
        </div>
      </ModalCard>

      <ModalCard open={successModal} onClose={setSuccesModal}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            height: "100%",
            textAlign: "center",
            justifyContent: "center",
            gap: "15px",
            flexDirection: "column",
          }}
        >
          <div> Mijoz bazaga muvaffaqiyatli qo'shildi </div>
          <GreenButton
            title="Ok"
            style={{
              background: "white",
              border: "1px solid #B6BCCD",
              color: "#325ECD",
            }}
            onClick={() => window.location.reload(false)}
          />
        </div>
      </ModalCard>
    </>
  );
};

export default HFAutocomplete;
