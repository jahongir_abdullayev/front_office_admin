import { TextField } from "@mui/material";
import { Controller } from "react-hook-form";

export const isNumber = (evt) => {
  evt = evt ? evt : window.event;
  let charCode = evt?.which ? evt?.which : evt?.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
    evt.preventDefault();
  } else {
    return true;
  }
};

const CNumberField = ({
  value,
  name,
  control,
  validation,
  onChange = () => {},
  customOnChange = () => {},
  textCenter,
  withoutBorder = true,
  enableNegative = false,
  ...props
}) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={validation}
      render={({ field: { onChange, value } }) => (
        <TextField
          {...props}
          value={value}
          // InputProps={{ inputProps: { min: 0, max: 10 } }}
          // inputProps={{
          //   style: { textAlign: textCenter ? "center" : "left" },
          //   readOnly: props.readOnly,
          // }}
          className={
            withoutBorder
              ? `product-table-input ${props.disabled ? "disabled-style" : ""}`
              : props.disabled
              ? "disabled-style"
              : ""
          }
          type="number"
          variant="outlined"
          onKeyPress={(e) => {
            if (!enableNegative) {
              isNumber(e);
            }
          }}
          onChange={(e) => {
            if (!e.target.value || e.target.value >= 0) {
              onChange(e);
            }

            customOnChange(e);
          }}
        />
      )}
    />
  );
};

export default CNumberField;
