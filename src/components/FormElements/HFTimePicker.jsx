import { DatePicker, DateTimePicker } from "@mui/lab";
import { TextField } from "@mui/material";
import { TimePicker } from "@mui/x-date-pickers";
import { Controller } from "react-hook-form";

const HFTimePicker = ({
  control,
  className,
  name,
  label,
  width,
  inputProps,
  disabledHelperText,
  placeholder,
  ...props
}) => {
  return (
    <Controller
      control={control}
      name={name}
      defaultValue=""
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <div className={className}>
          <TimePicker
            inputFormat="HH:mm"
            mask="__.__.____ __:__"
            toolbarFormat="HH:mm"
            value={value}
            name={name}
            onChange={onChange}
            {...props}
            renderInput={(params) => (
              <TextField
                {...params}
                style={{ width }}
                size="small"
                error={error}
                helperText={!disabledHelperText && (error?.message ?? " ")}
                label={label}
                inputProps={{
                  ...params.inputProps,
                  placeholder,
                }}
              />
            )}
          />
        </div>
      )}
    ></Controller>
  );
};

export default HFTimePicker;
