// import Select from "react-select";
import { t } from "i18next";
import { Controller } from "react-hook-form";
import MuiSelect from "../MuiSelect";
import cls from "./formComp.module.scss";

export default function FMuiSelect({
  label,
  height,
  minWidth,
  name,
  options,
  value,
  borderColor,
  placeholder,
  validation,
  defaultValue,
  isSearchable,
  error,
  register,
  disabled,
  errorMessage = true,
  style,
  control,
  customOnChange = (e) => {},
  isClearable,
  withBorder = true,
  ...props
}) {
  function optionsMake() {
    let arr = options?.map((el) => {
      if (typeof el === "object") {
        return {
          label: t(el?.title) || t(el?.label),
          value: el?.id || el?.value,
          disabled: el?.disabled ?? el?.disabled,
        };
      } else {
        return {
          label: t(el),
          value: el,
          disabled: el?.disabled ?? el?.disabled,
        };
      }
    });
    return arr;
  }
  const defVal = defaultValue?.value || defaultValue;

  return (
    <div className={cls.inputCont} style={{ ...style, height: height }}>
      <Controller
        name={name}
        control={control}
        rules={validation}
        defaultValue={defVal}
        render={({
          field: { onChange, onBlur, value, name, ref },
          fieldState: { error, isTouched },
        }) => (
          <MuiSelect
            height={height}
            minWidth={minWidth}
            borderColor={error ? "1px solid red" : borderColor}
            error={error}
            disabled={disabled}
            options={optionsMake()}
            placeholder={placeholder}
            label={label}
            value={value}
            onChange={(e) => {
              onChange(e);
              customOnChange(e, name);
            }}
            {...props}
          />
        )}
      />
    </div>
  );
}
