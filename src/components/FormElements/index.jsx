// import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
// import { DesktopDatePicker } from "@mui/lab";
import { Clear } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
// import { TextField } from "@mui/material";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { useTranslation } from "react-i18next";
// import { styled } from "@mui/material/styles";
import FTextField from "../FormComponentsV2/FTextField";
import DateFnsUtils from "@date-io/date-fns";

export default function CDatePicker({
  label,
  disabled,
  height,
  onChange,
  validation,
  withoutBorder,
  value = null,
  validate,
  error,
  maxDate,
  minDate,
  disableFuture,
  consolable,
  fullWidth,
  clearable = false,
  name,
  onClear = () => {},
  ...props
}) {
  const { t } = useTranslation();

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DesktopDatePicker
        label={label}
        value={value}
        disabled={disabled}
        inputFormat="dd/MM/yyyy"
        onChange={onChange}
        closeOnSelect={true}
        maxDate={maxDate}
        minDate={minDate}
        disableCloseOnSelect={false}
        disableFuture={disableFuture}
        style={{ height: "40px", width: props.width }}
        className="CheckedDate"
        // minDate={new Date("2017-01-01")}
        // PopperProps={{
        //   sx: popperSx,
        // }}
        {...props}
        renderInput={(params) => (
          <div style={{ position: "relative", display: "inline-block" }}>
            <FTextField
              {...params}
              errorFromDatePicker={error}
              consolable={consolable}
              style={{ height: "40px" }}
              variant="outlined"
              inputProps={{
                ...params.inputProps,
                placeholder: t("day.month.year"),
              }}
              sx={{
                input: {
                  "&::placeholder": {
                    fontWeight: 400,
                    fontSize: "12px",
                  },
                },
              }}
              fullWidth={fullWidth}
              className={withoutBorder ? "product-table-input" : ""}
              validation={{ required: true }}
            />
            {params && clearable ? (
              <IconButton
                style={{
                  position: "absolute",
                  top: "4px",
                  margin: "auto",
                  right: "2rem",
                }}
                onClick={() => onClear()}
              >
                <Clear />
              </IconButton>
            ) : (
              ""
            )}
          </div>
        )}
        {...props}
      />
    </LocalizationProvider>
  );
}
