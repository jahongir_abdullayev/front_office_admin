import { DatePicker } from "@mui/x-date-pickers";
import { IconButton, TextField } from "@mui/material";
import { Controller } from "react-hook-form";
import { Clear } from "@mui/icons-material";

const HFDatePicker = ({
  control,
  className,
  name,
  label,
  width = "100%",
  inputProps,
  disabledHelperText,
  placeholder,
  defaultValue,
  onClear = () => {},
  clearable = true,
  ...props
}) => {
  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      render={({ field: { onChange, value, name }, fieldState: { error } }) => (
        <div className={className}>
          <DatePicker
            inputFormat="dd.MM.yyyy"
            mask="__.__.____"
            toolbarFormat="dd.MM.yyyy"
            value={value}
            name={name}
            onChange={onChange}
            componentsProps={{
              actionBar: {
                actions: ["clear"],
              },
            }}
            onAccept={(newDate) => {}}
            {...props}
            renderInput={(params) => (
              <div
                style={{
                  position: "relative",
                  display: "inline-block",
                  width: "100%",
                }}
              >
                <TextField
                  {...params}
                  style={{
                    width: width || "",
                    height: "48px",
                    background: "white",
                    borderRadius: "6px",
                  }}
                  error={error}
                  helperText={!disabledHelperText && (error?.message ?? " ")}
                  {...inputProps}
                  inputProps={{
                    ...params.inputProps,
                    placeholder,
                  }}
                  label={label}
                />
                {clearable && (
                  <IconButton
                    style={{
                      position: "absolute",
                      top: "6px",
                      margin: "auto",
                      right: "20px",
                    }}
                    onClick={() => onClear(name, null)}
                  >
                    <Clear />
                  </IconButton>
                )}
              </div>
            )}
          />
        </div>
      )}
    ></Controller>
  );
};

export default HFDatePicker;
