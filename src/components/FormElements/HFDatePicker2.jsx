import { Controller } from "react-hook-form";
import CDatePicker from "./CDatePicker";
import cls from "./formComp.module.scss";

export default function FMuiDatePicker({
  label,
  name = "",
  placeholder,
  value,
  validation,
  defaultValue,
  height = "40px",
  width = "100%",
  error,
  register,
  disabled,
  withoutBorder,
  control,
  validate,
  disableFuture,
  fullWidth,
  maxDate,
  consolable,
  minDate,
  clearable,
  onClear = () => {},
  // height,
  // width,
  customOnChange = () => {},
  ...props
}) {
  return (
    <div className={cls.inputCont}>
      <Controller
        name={name}
        control={control}
        rules={validation}
        defaultValue={defaultValue}
        render={({
          field: { onChange, onBlur, value, name, ref },
          fieldState: { error },
        }) => (
          <CDatePicker
            error={error}
            label={label}
            maxDate={maxDate}
            minDate={minDate}
            withoutBorder={withoutBorder}
            disabled={disabled}
            disableFuture={disableFuture}
            fullWidth={fullWidth}
            name={name}
            consolable={consolable}
            onChange={(e) => {
              onChange(e);
              customOnChange(e);
            }}
            value={value}
            height={height}
            onClear={onClear}
            clearable={clearable}
            {...props}
          />
        )}
      />
    </div>
  );
}
