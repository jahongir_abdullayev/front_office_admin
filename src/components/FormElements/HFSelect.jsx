import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { height } from "@mui/system";
import { Controller } from "react-hook-form";

const HFSelect = ({
  control,
  name,
  label,
  width = "100%",
  options = [],
  disabledHelperText,
  placeholder,
  required = false,
  disabled = false,
  defaultValue = "",
  customChange = () => {},
  // height = "50px",
  rules = {},
  ...props
}) => {
  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={{
        required: required ? "This is required field" : false,
        ...rules,
      }}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <FormControl style={{ width }}>
          <InputLabel size="medium">{label}</InputLabel>
          <Select
            value={value || ""}
            label={label}
            // size="small"
            error={error}
            disabled={disabled}
            style={{ background: "white", borderRadius: "6px", height: height }}
            fullWidth
            onChange={(e) => {
              onChange(e.target.value);
              // customChange(e)
            }}
            {...props}
          >
            {options?.map((option) => (
              <MenuItem
                key={option.value}
                value={option.value}
                onClick={() =>
                  customChange({ value: option.value, label: option.label })
                }
              >
                {option.label}
              </MenuItem>
            ))}
          </Select>
          {/* {!disabledHelperText && (
            <FormHelperText error>{error?.message ?? " "}</FormHelperText>
          )} */}
        </FormControl>
      )}
    ></Controller>
  );
};

export default HFSelect;
