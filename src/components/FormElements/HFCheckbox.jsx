import { CheckCircle, CircleRounded } from "@mui/icons-material";
import { Checkbox } from "@mui/material";
import { useId } from "react";
import { Controller } from "react-hook-form";

const HFCheckbox = ({
  control,
  name,
  label,
  className,
  customChange = () => {},
  defaultValue = false,
}) => {
  const id = useId();

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <div
          className={className}
          style={{ display: "flex", alignItems: "center", gap: "5px" }}
        >
          <Checkbox
            id={`checkbox-${id}`}
            style={{
              transform: "translatey(-1px)",
              color: "#0EC30B",
              width: "30px",
            }}
            checked={value ?? false}
            onChange={(_, val) => {
              onChange(val);
              customChange(val);
            }}
            icon={
              <CircleRounded style={{ color: "#D9D9D9", fontSize: "30px" }} />
            }
            checkedIcon={<CheckCircle style={{ fontSize: "30px" }} />}
          />
          <label
            htmlFor={`checkbox-${id}`}
            style={{ fontSize: "20px", fontWeight: 600 }}
          >
            {label}
          </label>
        </div>
      )}
    ></Controller>
  );
};

export default HFCheckbox;
