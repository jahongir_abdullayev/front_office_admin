import { RadioGroup } from "@mui/material";
import { useEffect, useState } from "react";
import { Controller } from "react-hook-form";
import { useTranslation } from "react-i18next";
import RadioButton from "../RadioButton";
import cls from "./formComp.module.scss";
export default function HFRadioGroup({
  control,
  label,
  name = "",
  validation,
  style,
  elements = [],
  defaultValue = "",
  transportActiveIndex = 0,
  customOnChange = () => {},
}) {
  const { t, i18n } = useTranslation();
  const [radioElements, setRadioElements] = useState(elements);

  function onValueChange(value) {
    setRadioElements((prev) => {
      let clone = prev;
      clone.map((el) => {
        if (el.value === value) {
          el.checked = true;
        } else {
          el.checked = false;
        }
        return el;
      });
      return [...clone];
    });
  }

  useEffect(() => {
    setRadioElements(elements);
  }, [i18n, t, elements]);

  useEffect(() => {
    if (transportActiveIndex) onValueChange(transportActiveIndex.toString());
  }, [transportActiveIndex]);
  return (
    <div className={cls.radioGroup} style={style}>
      <label className={cls.radioGroupLabel}>{label}</label>
      <Controller
        control={control}
        name={name}
        defaultValue={defaultValue}
        rules={validation}
        render={({
          field: { onChange, onBlur, value, name, ref },
          fieldState: { error },
        }) => (
          <RadioGroup
            row
            value={value}
            onChange={(e, value) => {
              onChange(e);
              customOnChange(e, value);
              onValueChange(e.target.value);
            }}
          >
            {radioElements.map((el) => (
              <RadioButton
                key={el.label}
                value={el?.value}
                checked={el?.checked}
                label={el?.label}
              />
            ))}
          </RadioGroup>
        )}
      />
    </div>
  );
}
