import { styled } from "@material-ui/styles";
import { Search } from "@mui/icons-material";
import { CircularProgress, TextField } from "@mui/material";
import { Controller } from "react-hook-form";

const CustomTextField = styled(TextField)({
  "& .MuiInputBase-root.Mui-disabled": {
    backgroundColor: "#F4F6FA",
  },
});

const HFTextField = ({
  control,
  name = "",
  disabledHelperText = false,
  required = false,
  rules = {},
  width = "100%",
  placeholder = "",
  searchButton = false,
  onClick = () => {},
  customOnChange = () => {},
  loading = false,
  height = "48px",
  errorText = "This is required field",
  type = "text",
  ...props
}) => {
  const SearchButton = () => (
    <div
      onClick={onClick}
      style={{
        cursor: "pointer",
        background: "#325ECD",
        height: "100%",
        display: "flex",
        alignItems: "center",
        padding: "0 18px",
        borderTopRightRadius: "6px",
        borderBottomRightRadius: "6px",
      }}
    >
      {loading ? (
        <CircularProgress
          style={{ color: "white", width: "18.5px", height: "18.5px" }}
        />
      ) : (
        <Search style={{ color: "white", fontSize: "20px" }} />
      )}
    </div>
  );

  return (
    <Controller
      control={control}
      name={name}
      defaultValue=""
      rules={{
        required: required ? errorText : false,
        ...rules,
      }}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <CustomTextField
          // size="small"
          value={value}
          label={placeholder}
          onChange={(e) => {
            onChange(e.target.value);
            customOnChange(e);
          }}
          style={{
            background: "white",
            height: height,
            borderRadius: "6px",
            width: width,
          }}
          name={name}
          error={error}
          InputProps={{ endAdornment: searchButton ? <SearchButton /> : "" }}
          helperText={!disabledHelperText && (error?.message ?? " ")}
          type={type}
          inputProps={{
            inputMode: type === "number" ? "numeric" : undefined,
            pattern: type === "number" ? "[0-9]*" : undefined,
          }}
          {...props}
        />
      )}
    ></Controller>
  );
};

export default HFTextField;
