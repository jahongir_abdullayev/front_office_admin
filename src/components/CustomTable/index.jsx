import { Paper } from "@mui/material";
import { forwardRef } from "react";
import CPagination from "../CPagination";
import EmptyDataComponent from "../EmptyDataComponent";
import TableLoader from "../TableLoader";
import "./style.scss";

export const CustomTable = ({
  children,
  count,
  page,
  setCurrentPage,
  removableHeight = 186,
  disablePagination,
}) => {
  return (
    <Paper className="CustomTableContainer">
      <div
        className="table"
        style={{
          height: removableHeight
            ? `calc(100vh - ${removableHeight}px)`
            : "auto",
          border: "none",
        }}
      >
        <table>{children}</table>
      </div>

      {!disablePagination && (
        <CPagination
          count={count}
          page={page}
          setCurrentPage={setCurrentPage}
        />
      )}
    </Paper>
  );
};

export const CustomTableHead = ({ children }) => {
  return <thead className="CustomTableHead">{children}</thead>;
};

export const CustomTableHeadRow = ({ children, style }) => {
  return (
    <tr className="CTableHeadRow" style={{ ...style }}>
      {children}
    </tr>
  );
};

export const CustomTableBody = forwardRef(
  ({ children, columnsCount, loader, dataLength, ...props }, ref) => {
    return (
      <tbody className="CustomTableBody" {...props} ref={ref}>
        {!loader && children}
        {/* <EmptyDataComponent isVisible={!loader && !dataLength}  /> */}
        <TableLoader isVisible={loader} columnsCount={columnsCount} />
      </tbody>
    );
  }
);

export const CustomTableRow = ({ children, ...props }) => {
  return (
    <tr className="CustomTableRow" {...props}>
      {children}
    </tr>
  );
};

export const CustomTableCell = ({
  children,
  className = "",
  minWidth = "",
  border = false,
  ...props
}) => {
  return (
    <td
      className={`CustomTableCell ${className}`}
      style={{
        minWidth: minWidth,
        ...props?.style,
        borderLeft: border ? "3px solid #EEF4FF" : "",
        borderBottom: border ? "3px solid #EEF4FF" : "",
      }}
      {...props}
    >
      {children}
    </td>
  );
};
