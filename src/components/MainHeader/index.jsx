import styles from "./style.module.scss";
import ava from "../../assets/images/3x4.jpg";
import { Avatar } from "@mui/material";
import { useSelector } from "react-redux";

const MainHeader = ({
  title = "",
  subtitle,
  extra,
  children,
  loader,
  backButtonLink,
  icon,
  sticky,
  ...props
}) => {
  const user = JSON.parse(localStorage.getItem("userInfo"))?.email;
  return (
    <div className={styles.header}>
      <div className={styles.leftSide}></div>
      <div className={styles.rightSide}>
        <div className={styles.infoBlock}>
          <div style={{ width: "100%" }}>
            <div className={styles.name}> {user} </div>
          </div>
          <Avatar
            alt="Beketov Galimjan"
            src={ava}
            sizes="large"
            sx={{ width: 56, height: 56 }}
          />
        </div>
      </div>
    </div>
  );
};

export default MainHeader;
