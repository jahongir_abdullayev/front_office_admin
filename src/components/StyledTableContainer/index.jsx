import { Paper, TableContainer } from "@material-ui/core";
// import "./style.scss";

export const StyledTableContainer = (props) => {
  return (
    <div style={props.style} className={`styled-table ${props.className}`}>
      <TableContainer
        style={{ height: props?.height ? props?.height : "auto" }}
      >
        {props.children}
      </TableContainer>
    </div>
  );
};
