import { Pagination, PaginationItem } from "@mui/material"
import { DownloadIcon, NextIcon, PreviousIcon } from "../../utils/icons"
import HFSelect from "../FormElements/HFSelect";


const CPagination = ({ setCurrentPage = () => {}, ...props }) => {
  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        alignItems: "center",
        marginTop: "15px",
        justifyContent: "space-between",
      }}
    >
      <Pagination
        count={10}
        renderItem={(item) => (
          <PaginationItem
            components={{
              // last: (props) => <button {...props}>Last</button>,
              next: (props) => (
                <button
                  style={{
                    display: "flex",
                    alignItems: "center",
                    background: "white",
                    border: "none",
                    padding: "8px 12px",
                    borderRadius: "8px",
                    gap: "10px",
                  }}
                  {...props}
                >
                  Keyingisi <NextIcon />
                </button>
              ),
              // first: (props) => <button {...props}>First</button>,
              previous: (props) => (
                <button
                  style={{
                    display: "flex",
                    alignItems: "center",
                    background: "white",
                    border: "none",
                    padding: "8px 12px",
                    borderRadius: "8px",
                    gap: "10px",
                  }}
                  {...props}
                >
                  <PreviousIcon /> Oldingi
                </button>
              ),
            }}
            {...item}
          />
        )}
        color="primary"
        shape="rounded"
        onChange={(e, val) => setCurrentPage(val)}
        {...props}
      />
      <div style={{display: 'flex', gap: '14px', alignItems: 'center'}}>
       
      <div
        style={{
          color: "#325ECD",
          background: "white",
          padding: "8.5px 20px",
          borderRadius: "8px",
          display: 'flex',
          alignItems: 'center',
          gap: '10px',
          fontWeight: 700,
          // fontSize: '17px'
        }}
      >
        <DownloadIcon /> Yuklab olish
      </div>
      </div>
    </div>
  );
}

export default CPagination
