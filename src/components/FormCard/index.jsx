import { Typography } from "@mui/material"
import "./style.scss"

const FormCard = ({ visible = true, children, title, className, extra, maxWidth = 700, contentPadding = '20px', height = 'auto' , ...props }) => {
  if(!visible) return null

  return (
    <div className={`FormCard ${className}`} style={{ maxWidth, height: height }} >
      <div className="card" {...props} >
          {title && <div className="header">
           <Typography variant="h4" className="title" >{ title }</Typography>
           <div className="extra">{extra}</div>
          </div>}
          <div className="content" style={{padding: contentPadding }}>
            { children }
          </div>
      </div>
    </div>
  )
}

export default FormCard
