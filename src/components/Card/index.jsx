import cls from "./card.module.scss";

export default function Card({
  title,
  children,
  extra,
  className = "",
  style,
  headerStyle,
  filterStyle = "",
  headerClass = "",
  bodyStyle,
  bodyClass,
  filters,
  boldTitle,
  footer,
  footerStyle,
  ...args
}) {
  return (
    <div className={cls.cardCont} style={style} {...args}>
      {(title || extra) && (
        <div style={headerStyle} className={cls.title}>
          {title}
          <div>{extra}</div>
        </div>
      )}
      {children && children !== "" ? (
        <div className={cls.body} style={bodyStyle}>
          {children}
        </div>
      ) : (
        ""
      )}
      {footer && (
        <div className={cls.footer} style={footerStyle}>
          {footer}
        </div>
      )}
    </div>
  );
}
