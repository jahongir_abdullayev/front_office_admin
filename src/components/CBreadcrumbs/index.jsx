import NavigateNextIcon from "@mui/icons-material/NavigateNext"
import { Breadcrumbs } from "@mui/material"
import { useMemo } from "react"
import { useNavigate } from "react-router-dom"
import { BreadCrumbIcon } from "../../utils/icons"
import BackButton from "../BackButton"
import "./style.scss"

const CBreadcrumbs = ({
  icon,
  withDefautlIcon = true,
  size,
  className,
  items,
  type = 'element',
}) => {
  const navigate = useNavigate()

  const navigateLink = useMemo(() => {
    if(type !== 'link') return null
    return items[items.length - 2]?.link ?? null
  }, [items, type])


  const navigateHandler = (link, index) => {
    if (index === items?.length - 1) return null
    navigate(link)
  }

  return (
    <div className="CBreadcrumbs-wrapper">
      {navigateLink && <BackButton link={navigateLink} />}
      {withDefautlIcon && (
        <div onClick={() => navigate(-1)} style={{ cursor: "pointer" }}>
          {" "}
          <BreadCrumbIcon />{" "}
        </div>
      )}
      <Breadcrumbs
        className={`CBreadcrumbs ${size} ${className}`}
        // separator={<NavigateNextIcon fontSize="small" />}
      >
        {items?.map((item, index) => (
          <div key={index} className={`breadcrumb-item ${type}`}>
            {icon}

            {type === "link" ? (
              <div
                onClick={() => navigateHandler(item.link, index)}
                className="breadcrumb-label"
              >
                {item.label}
              </div>
            ) : (
              <div
                className="breadcrumb-label"
                style={{
                  color: item.color,
                  fontWeight: item.color !== "black" && 500,
                }}
              >
                {item.label}
              </div>
            )}
          </div>
        ))}
      </Breadcrumbs>
    </div>
  );
}

export default CBreadcrumbs
