import {
  TableCell,
  TableRow,
  withStyles,
  TableHead,
  TableBody,
  Table,
} from "@material-ui/core";

export const StyledTableCell = withStyles((theme) => ({
  head: {
    border: "1px solid #E5E9EB",
    backgroundColor: "#e0e9f5",
    fontWeight: "bold",
    fontSize: "17px",
    lineHeight: "22px",
    color: "#1A2024",
    whiteSpace: "nowrap",
    fontFamily: "Inter !important",
  },

  root: {
    border: "1px solid #E5E9EB",
    color: "#252C32",
    fontWeight: "400",
    fontSize: "14px",
    lineHeight: "24px",
    padding: "0px 16px !important",
    fontFamily: "Inter",
    height: "48px",
    minHeight: "48px",
  },
}))(TableCell);

export const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(even)": {
      backgroundColor: "#F4F6FA",
    },
  },
}))(TableRow);
export const StyledTableHead = withStyles((theme) => ({
  root: {
    fontWeight: "bold",
    fontSize: "16px",
  },
}))(TableHead);

export const StyledTableBody = withStyles((theme) => ({
  root: {
    "&:last-child": {
      borderTopRightRadius: "12px",
    },
  },
}))(TableBody);
export const StyledTable = withStyles((theme) => ({
  root: {
    borderCollapse: "separate",
    borderSpacing: "0px !important",
    border: "0.5px solid #E5E9EB",
    borderRadius: "8px",
    overflow: "hidden",
  },
}))(Table);

// export const StyledTableContainer = withStyles((theme) => ({
//   root: {},
// }))(TableContainer);
