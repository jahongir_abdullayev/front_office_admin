import { Pagination } from "@mui/material";
import { useMemo, useState } from "react";
import { CheverDown, DocIcon } from "../../utils/icons";
// import { CheverDown, DocIcon } from "../../assets/icons/icons";
const MPagination = ({
  count,
  limit = 10,
  page = 1,
  docCount,
  setCurrentPage = () => {},
  setLimit = () => {},
  disabledDocCount = true,
  allCount,
  currentPage,
  isSpecial = false,
  ...props
}) => {
  const [showOptions, setShowOptions] = useState(false);
  const limitCount = useMemo(() => {
    if (isSpecial) {
      return [10, 500, 1000, 5000];
    } else return [10, 50, 100, 200];
  }, [isSpecial]);

  if (currentPage === undefined) {
    if (page) {
      currentPage = page;
    } else {
      currentPage = 1;
    }
  }
  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          columnGap: "10px",
        }}
      >
        <div
          style={{
            cursor: "pointer",
            display: "flex",
            alignItems: "center",
            gap: "10px",
            padding: "6px",
            position: "relative",
            border: "1px solid #eee",
            borderRadius: "6px",
          }}
          onClick={() => setShowOptions(!showOptions)}
        >
          <DocIcon />
          <span
            style={{
              color: "#303940",
              fontWeight: "500",
              fontSize: "14px",
              userSelect: "none",
            }}
          >
            {limit}-тадан кўрсатиш
          </span>
          <span style={{ display: "flex", alignItems: "center" }}>
            <CheverDown />
          </span>
          {showOptions && (
            <div
              className="absolute flex flex-col bg-white w-full border rounded-md"
              style={{ bottom: "-130px", left: "0" }}
            >
              {limitCount.map((limitValue) => (
                <div
                  className=" border-b p-2 text-center hover:bg-gray-100"
                  key={limitValue}
                  onClick={() => setLimit(limitValue)}
                >
                  {limitValue}-тадан кўрсатиш
                </div>
              ))}
            </div>
          )}
        </div>
        {disabledDocCount && allCount && (
          <div className="flex gap-2">
            <p>
              {limit * currentPage - limit + 1 > allCount
                ? 1
                : limit * currentPage - limit + 1}
              &nbsp;дан{" "}
              {limit * currentPage > allCount ? allCount : limit * currentPage}
              &nbsp;гача кўрилмоқда. Жами {allCount} та
            </p>
          </div>
        )}
      </div>
      <Pagination
        color="primary"
        shape="rounded"
        count={count}
        page={page}
        onChange={(e, val) => {
          console.log("vale", val);
          setCurrentPage(val);
        }}
        {...props}
      />
    </div>
  );
};

export default MPagination;
