import { memo, useEffect } from "react";
import { useDispatch } from "react-redux";
import { espActions } from "../store/esp/esp.slice";

const EIMZOClient = window.EIMZOClient;
const EIMZO_MAJOR = 3;
const EIMZO_MINOR = 27;

if (EIMZOClient) {
  EIMZOClient.API_KEYS = [
    "localhost",
    "96D0C1491615C82B9A54D9989779DF825B690748224C2B04F500F370D51827CE2644D8D4A82C18184D73AB8530BB8ED537269603F61DB0D03D2104ABF789970B",
    "127.0.0.1",
    "A7BCFA5D490B351BE0754130DF03A068F855DB4333D43921125B9CF2670EF6A40370C646B90401955E1F7BC9CDBF59CE0B2C5467D820BE189C845D0B79CFC96F",
    "null",
    "E0A205EC4E7B78BBB56AFF83A733A1BB9FD39D562E67978CC5E7D73B0951DB1954595A20672A63332535E13CC6EC1E1FC8857BB09E0855D7E76E411B6FA16E9D",
    "dls.yt.uz",
    "EDC1D4AB5B02066FB3FEB9382DE6A7F8CBD095E46474B07568BC44C8DAE27B3893E75B79280EA82A38AD42D10EA0D600E6CE7E89D1629221E4363E2D78650516",
    "new.soliqservis.uz",
    "0A521B36D2A97FD4E6F62E5EC8F6BA0D9E0C4D852ADC995E027EFD2A982885CBA3F2B798504013EAD8759CF7D5FA0E961133EF408AC49E1EA40FAFE20AC22FF0",
  ];
}

const ESPProvider = () => {
  const dispatch = useDispatch();
  // ================CHECK VERSION==========================

  EIMZOClient.idCardIsPLuggedIn(
    function (token) {
      console.log(token);
      dispatch(espActions.setToken(token?.readers));
    },
    function (e, r) {
      if (e) {
        // uiShowMessage("err" + " : " + e);
      } else {
        console.log(r);
      }
    }
  );

  // ==================LOAD KEYS=======================

  // ==================CHECK EXPIRE=================
};

export default memo(ESPProvider);
