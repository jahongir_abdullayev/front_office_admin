import LoginForm from "./LoginForm";
import styles from "./style.module.scss";
import logo from "/tatu.jpg";

const AuthLayout = () => {
  return (
    <div className={styles.layout}>
      <header className={styles.header}>
        <img
          src={logo}
          alt="tatu-logo"
          style={{
            borderRadius: "50%",
            objectFit: "cover",
          }}
          width={69}
          height={69}
        />
        <span>TATU</span>
      </header>
      <div className={styles.container}>
        <LoginForm />
      </div>
    </div>
  );
};

export default AuthLayout;
