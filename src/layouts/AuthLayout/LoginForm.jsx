import React from "react";
// import useRequest from "../../hooks/useRequest";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { useFirebase } from "../../FirebaseProvider/FirebaseProvider";
import HFTextField from "../../components/FormElements/HFTextField";
import { useForm } from "react-hook-form";
import { Button } from "@mui/material";
import { useDispatch } from "react-redux";
import { authActions } from "../../store/auth/auth.slice";

const LoginForm = () => {
  const navigate = useNavigate();
  const { signIn } = useFirebase();
  const { control, watch, handleSubmit, setValue } = useForm();
  //   const { isLoading, mutate, Spinner } = useRequest(signIn, {
  //     onError: (err) => {
  //       toast.error(`Kirishda xatolik yuz berdi: ${err.message}`);
  //     },
  //     onSuccess() {
  //       navigate("/");
  //     },
  //   });

  const dispatch = useDispatch();

  const onSubmit = (e) => {
    console.log(e);
    signIn(e)
      .then((response) => {
        // dispatch(authActions.login(response));
        const body = {
          ...response,
          isAuth: true,
        };
        localStorage.setItem("userInfo", JSON.stringify(body));
      })
      .catch(() => {
        // console.log("err", err);
      });
    // mutate({ email, password });
  };

  return (
    <div className="login-form-box">
      <form className="login-form" onSubmit={handleSubmit(onSubmit)}>
        <div style={{ width: "400px", marginBottom: "20px" }}>
          <HFTextField control={control} name="email" placeholder="Email" />
        </div>
        <div style={{ width: "400px", marginBottom: "20px" }}>
          <HFTextField
            control={control}
            name="password"
            placeholder="Parol"
            type="password"
          />
        </div>

        <Button
          type="submit"
          fullWidth
          variant="contained"
          size="large"
          sx={{ background: "#005fc4" }}
        >
          Kirish
        </Button>
      </form>
    </div>
  );
};

export default LoginForm;
