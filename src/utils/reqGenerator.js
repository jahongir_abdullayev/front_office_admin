import axios from "axios";
// import { store } from "../redux/store";
// import { showAlert } from "../redux/thunks/alert.thunk";
import { store } from "../store";
import { showAlert } from "../store/alert/alert.thunk";
// export const baseURL = "http://80.80.218.145:9000/api"
export const baseURL = import.meta.env.VITE_BASE_URL;

const reqGenerator = axios.create({
  baseURL,
  timeout: 100000,
});

const errorHandler = (error, hooks) => {
  if (error?.response) {
    if (error.response?.data?.errorMessage) {
      store.dispatch(
        showAlert(error.response.data.errorMessage, "error", 6000)
      );
    }

    if (error?.response?.status === 403) {
    } else if (error?.response?.status === 401) {
      // store.dispatch(logout())
    }
  } else {
    // store.dispatch(showAlert("_______Error________", "error"));
  }

  return Promise.reject(error.response);
};

reqGenerator.interceptors.request.use(
  (config) => {
    // const token = store.getState().auth.accessToken
    // if(token) {
    // config.headers.Authorization = token
    // }
    return config;
  },

  (error) => errorHandler(error)
);

reqGenerator.interceptors.response.use(
  (response) => response.data,
  errorHandler
);

export default reqGenerator;
