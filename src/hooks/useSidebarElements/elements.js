import { NavbarDashboardIcon } from "../../utils/icons";
import Articles from "../../views/Articles";
import ArticleCreate from "../../views/Articles/Form";
import Courses from "../../views/Courses";
import CourseCreate from "../../views/Courses/Form";
import MainPage from "../../views/MainPage";
import Teachers from "../../views/Teachers";
import TeacherCreate from "../../views/Teachers/Form";

export const elements = [
  // {
  //   id: "positions",
  //   title: "Bosh sahifa",
  //   path: "/main",
  //   icon: NavbarDashboardIcon,
  //   component: MainPage,
  // },
  {
    id: "teachers",
    title: "O'qituvchilar",
    path: "/teachers",
    icon: NavbarDashboardIcon,
    component: Teachers,
  },
  {
    id: "articles",
    title: "Nashrlar",
    path: "/articles",
    icon: NavbarDashboardIcon,
    component: Articles,
  },
  {
    id: "courses",
    title: "Kurslar",
    path: "/courses",
    icon: NavbarDashboardIcon,
    component: Courses,
  },
];

export const pages = [
  {
    id: "teachers",
    title: "Yangi o'qituvchi qo'shish",
    path: "/teachers/create",
    component: TeacherCreate,
  },
  {
    id: "articles",
    title: "Yangi nashr qo'shish",
    path: "/articles/create",
    component: ArticleCreate,
  },
  {
    id: "courses",
    title: "Kurslar qo'shish",
    path: "/courses/create/:courseId",
    component: CourseCreate,
  },
];
