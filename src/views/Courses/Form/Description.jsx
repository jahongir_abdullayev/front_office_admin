import { useState } from "react";
import { useTranslation } from "react-i18next";
import cls from "./createCustom.module.scss";
import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage";

export default function Description({ control, setValue, watch }) {
  const { t } = useTranslation();
  const [file, setFile] = useState();
  const bar = document.getElementById("progress-bar");
  const [selectedFile, setSelectedFile] = useState(null);

  const UploadFile = async (e) => {
    const file = e[0];
    const name = watch("fullName");
    try {
      const storage = getStorage(); // Get Firebase Storage instance
      const storageRef = ref(storage, `teachers/${name}`); // Reference to the destination path in Firebase Storage

      // Upload the image file to Firebase Storage
      await uploadBytes(storageRef, file);
      const imageUrl = await getDownloadURL(storageRef);
      console.log("Image URL:", imageUrl);
      setValue("imgUrl", imageUrl);
    } catch (error) {
      console.error("Error uploading image:", error);
    }

    // var reader = new FileReader();
    // reader.readAsBinaryString(file);
    // reader.onloadend = function () {
    //   var count = reader.result.match(/\/Type[\s]*\/Page[^s]/g).length;
    //   console.log("Number of Pages:", count);
    // };
    // setFile(e[0].name);
    // const data = new FormData();
    // data.append("file", file);

    // const config = {
    //   onUploadProgress: function (progressEvent) {
    //     const percentCompleted = Math.round(
    //       (progressEvent.loaded / progressEvent.total) * 100
    //     );
    //     bar.setAttribute("value", percentCompleted);
    //     bar.previousElementSibling.textContent = `${percentCompleted}%`;
    //     if (percentCompleted === 100) {
    //       bar.previousElementSibling.textContent = `${t("upload.complete")}`;
    //     }
    //   },
    // };
  };

  return (
    <div className={cls.uploadBox}>
      <input
        type="file"
        name="File"
        id="img"
        style={{ display: "none" }}
        accept="video/"
        onChange={(e) => UploadFile(e.target.files)}
      />
      <label for="img">
        <div style={{ textAlign: "center" }}>
          {/* <CustomUploadIcon />  */}
          <div> Rasmni yuklash </div>
          <div className={cls.upload_button}> Yuklash </div>
        </div>
        {file ? <div> {file} </div> : ""}
        <progress id="progress-bar" value="0" max="100"></progress>
      </label>
    </div>
  );
}
