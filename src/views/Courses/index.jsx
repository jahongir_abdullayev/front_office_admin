import { useCallback, useEffect, useState } from "react";
import "./Teachers.scss";
import MPagination from "../../components/Pagination/MPagination";
import { StyledTableContainer } from "../../components/StyledTableContainer";
import {
  StyledTable,
  StyledTableBody,
  StyledTableCell,
  StyledTableHead,
  StyledTableRow,
} from "../../components/Styled";
import Card from "../../components/Card";
import { useFirebase } from "../../FirebaseProvider/FirebaseProvider";
import { DeleteIcon, EditIcon } from "../../utils/icons";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const Courses = () => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const { getDocuments, deleteDocument } = useFirebase();
  const navigate = useNavigate();

  const GetData = useCallback(() => {
    getDocuments({
      collectionName: "courses",
      withDocumentId: true,
      filters: [],
    })
      .then((res) => {
        console.log(res);
        setData(res);
      })
      .catch((err) => {
        console.log("error", err);
      });
  }, [getDocuments]);

  const [courses, setCourses] = useState([]);

  const GetCourses = useCallback(() => {
    getDocuments({
      collectionName: "course_types",
      withDocumentId: true,
      filters: [],
    })
      .then((res) => {
        setCourses(res);
      })
      .catch((err) => {
        console.log("error", err);
      });
  }, [getDocuments]);

  useEffect(() => {
    GetData();
    GetCourses();
  }, [GetData, GetCourses]);

  function deleteTeacher(id) {
    deleteDocument({ collectionName: "courses", id: id })
      .then((res) => {
        GetData();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function getValue(value) {
    switch (value) {
      case 0:
        return "ZbQNnKxyUmeEJZ6NTOzV";

      case 1:
        return "ZbQNnKxyUmeEJZ6NTOzV";

      default:
        return "ZbQNnKxyUmeEJZ6NTOzV";
    }
  }

  console.log("datadatadata", data);

  return (
    <div>
      <div className="mainContainer">
        <div className="mt-4">
          <Card
            style={{ boxShadow: "0px 20px 20px rgba(135, 138, 146, 0.16)" }}
            footerStyle={{ borderTop: "1px solid #eee" }}
            extra={
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                <Button
                  variant="contained"
                  onClick={() => {
                    navigate(`/courses/create/${getValue(value)}`);
                  }}
                >
                  Yaratish
                </Button>
              </div>
            }
            footer={
              <>
                {data?.length > 0 && (
                  <MPagination
                    count={Math.ceil(data?.length / 10)}
                    allCount={count}
                    page={1}
                    setCurrentPage={setPage}
                  />
                )}
              </>
            }
          >
            {/* <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
              <Tabs
                value={value}
                onChange={handleChange}
                aria-label="basic tabs example"
              >
                {courses?.map((el) => (
                  <Tab label={el?.course_type} {...a11yProps(el?.id)} />
                ))}
                {/* <Tab label="Item One" {...a11yProps(0)} />
                <Tab label="Item Two" {...a11yProps(1)} />
                <Tab label="Item Three" {...a11yProps(2)} /> */}
            {/* </Tabs>
            </Box> */}
            {/* <TabPanel value={value} index={0}> */}
            <StyledTableContainer>
              <StyledTable>
                <StyledTableHead>
                  <StyledTableRow>
                    <StyledTableCell width={48}>№</StyledTableCell>
                    <StyledTableCell>O'qituvchi</StyledTableCell>
                    <StyledTableCell>Email</StyledTableCell>
                    <StyledTableCell>Telefon raqami</StyledTableCell>
                    <StyledTableCell width={48}></StyledTableCell>
                  </StyledTableRow>
                </StyledTableHead>
                <StyledTableBody>
                  {data?.length > 0 ? (
                    data?.map((item, idx) => (
                      <StyledTableRow key={item?.id}>
                        <StyledTableCell>{idx + 1}</StyledTableCell>
                        <StyledTableCell>{item?.fullName}</StyledTableCell>
                        <StyledTableCell width={300}>
                          {item?.email}
                        </StyledTableCell>
                        <StyledTableCell>
                          {item?.phone || "+998917776842"}
                        </StyledTableCell>
                        <StyledTableCell align="center">
                          <div
                            style={{
                              display: "flex",
                              alignItems: "center",
                              gap: "10px",
                            }}
                          >
                            <div
                              className="bg-white border p-2 rounded-md cursor-pointer"
                              onClick={() => {}}
                            >
                              <EditIcon />
                            </div>
                            <div
                              className="bg-white border p-2 rounded-md cursor-pointer"
                              onClick={() => {
                                deleteTeacher(item?.id);
                              }}
                            >
                              <DeleteIcon />
                            </div>
                          </div>
                        </StyledTableCell>
                      </StyledTableRow>
                    ))
                  ) : (
                    <></>
                  )}
                </StyledTableBody>
              </StyledTable>
            </StyledTableContainer>
            {/* </TabPanel> */}
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Courses;
