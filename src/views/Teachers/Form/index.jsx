import { useCallback, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Card from "../../../components/Card";
import "./style.scss";
import TeachersForm from "./formComp";
import { Button } from "@mui/material";
import { useForm } from "react-hook-form";
import Firebase from "../../../FirebaseProvider/Firebase";
import { config } from "../../../FirebaseProvider/constants";

const TeacherCreate = () => {
  const { id } = useParams();
  const { control, watch, handleSubmit, setValue } = useForm();
  const [description, setDescription] = useState();
  const navigate = useNavigate();
  const [firestore] = useState(() => new Firebase(config));

  function create(data) {
    firestore
      .createDocument(data, { collectionName: "teachers" })
      .then((res) => {
        navigate("/teachers");
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function update(data) {}
  const GetData = useCallback(() => {
    if (id) {
    }
  }, [id]);

  useEffect(() => {
    GetData();
  }, [GetData]);

  const onSubmit = (values) => {
    const data = {
      ...values,
      description: description,
    };
    if (id?.length) {
    } else {
      create(data);
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="mainContainer">
        <Card
          extra={
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <Button
                variant="contained"
                onClick={() => {
                  // navigate("/teachers/create");
                }}
                type="submit"
              >
                Yaratish
              </Button>
            </div>
          }
        >
          <TeachersForm
            control={control}
            description={description}
            setDescription={setDescription}
            watch={watch}
            setValue={setValue}
          />
        </Card>
      </div>
    </form>
  );
};

export default TeacherCreate;
