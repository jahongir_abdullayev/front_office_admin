import { useLocation } from "react-router-dom";
import IconButton from "@mui/material/IconButton";
import { Clear } from "@material-ui/icons";
import { useState } from "react";
import { Grid } from "@mui/material";
import HFSelect from "../../../components/FormElements/HFSelect";
import HFTextField from "../../../components/FormElements/HFTextField";
import MyStatefulEditor from "./Editor";
import Description from "./Description";
// import EditorComponent from "./Editor";

export default function TeachersForm({
  control,
  setDescription,
  watch,
  setValue,
}) {
  const onChange = (value) => {
    setDescription(value);
  };

  return (
    <div className="container">
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <HFTextField name="fullName" control={control} placeholder="FIO" />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="email"
            control={control}
            placeholder="Email pochta"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="phone"
            control={control}
            placeholder="Telefon raqami"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="subject"
            control={control}
            placeholder="O'qitadigan fan nomi"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="position"
            control={control}
            placeholder="O'qituvchi martabasi"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="url.facebook"
            control={control}
            placeholder="FaceBook link"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="url.youtube"
            control={control}
            placeholder="YouTube link"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="url.linkedin"
            control={control}
            placeholder="Linkedin link"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="url.instagram"
            control={control}
            placeholder="Instagram link"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="url.twitter"
            control={control}
            placeholder="Twitter link"
          />
        </Grid>
        <Grid item xs={12}>
          <Description watch={watch} setValue={setValue} />
        </Grid>
        <Grid item xs={12}>
          <div style={{ height: "400px" }}>
            <h3>O'qituvchi ma'lumoti</h3>
            <MyStatefulEditor markup="" onChange={onChange} />
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
