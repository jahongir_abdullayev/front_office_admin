import { useCallback, useEffect, useState } from "react";
import "./Teachers.scss";
import MPagination from "../../components/Pagination/MPagination";
import { StyledTableContainer } from "../../components/StyledTableContainer";
import {
  StyledTable,
  StyledTableBody,
  StyledTableCell,
  StyledTableHead,
  StyledTableRow,
} from "../../components/Styled";
import Card from "../../components/Card";
import { useFirebase } from "../../FirebaseProvider/FirebaseProvider";
import { DeleteIcon, EditIcon } from "../../utils/icons";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";

const Teachers = () => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const { getDocuments, deleteDocument } = useFirebase();
  const navigate = useNavigate();

  const GetData = useCallback(() => {
    getDocuments({
      collectionName: "teachers",
      withDocumentId: true,
      filters: [],
    })
      .then((res) => {
        setData(res);
      })
      .catch((err) => {
        console.log("error", err);
      });
  }, [getDocuments]);

  useEffect(() => {
    GetData();
  }, [GetData]);

  function deleteTeacher(id) {
    deleteDocument({ collectionName: "teachers", id: id })
      .then((res) => {
        GetData();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <div>
      <div className="mainContainer">
        <div className="mt-4">
          <Card
            style={{ boxShadow: "0px 20px 20px rgba(135, 138, 146, 0.16)" }}
            footerStyle={{ borderTop: "1px solid #eee" }}
            extra={
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                <Button
                  variant="contained"
                  onClick={() => {
                    navigate("/teachers/create");
                  }}
                >
                  Yaratish
                </Button>
              </div>
            }
            footer={
              <>
                {data?.length > 0 && (
                  <MPagination
                    count={Math.ceil(data?.length / 10)}
                    allCount={count}
                    page={1}
                    setCurrentPage={setPage}
                  />
                )}
              </>
            }
          >
            <StyledTableContainer>
              <StyledTable>
                <StyledTableHead>
                  <StyledTableRow>
                    <StyledTableCell width={48}>№</StyledTableCell>
                    <StyledTableCell>FIO</StyledTableCell>
                    <StyledTableCell>Email</StyledTableCell>
                    <StyledTableCell>Telefon raqami</StyledTableCell>
                    <StyledTableCell width={48}></StyledTableCell>
                  </StyledTableRow>
                </StyledTableHead>
                <StyledTableBody>
                  {data?.length > 0 ? (
                    data?.map((item, idx) => (
                      <StyledTableRow key={item?.id}>
                        <StyledTableCell>{idx + 1}</StyledTableCell>
                        <StyledTableCell>{item?.fullName}</StyledTableCell>
                        <StyledTableCell width={300}>
                          {item?.email}
                        </StyledTableCell>
                        <StyledTableCell>
                          {item?.phone || "+998917776842"}
                        </StyledTableCell>
                        <StyledTableCell align="center">
                          <div
                            style={{
                              display: "flex",
                              alignItems: "center",
                              gap: "10px",
                            }}
                          >
                            <div
                              className="bg-white border p-2 rounded-md cursor-pointer"
                              onClick={() => {}}
                            >
                              <EditIcon />
                            </div>
                            <div
                              className="bg-white border p-2 rounded-md cursor-pointer"
                              onClick={() => {
                                deleteTeacher(item?.id);
                              }}
                            >
                              <DeleteIcon />
                            </div>
                          </div>
                        </StyledTableCell>
                      </StyledTableRow>
                    ))
                  ) : (
                    <></>
                  )}
                </StyledTableBody>
              </StyledTable>
            </StyledTableContainer>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Teachers;
