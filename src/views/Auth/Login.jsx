import { useNavigate } from "react-router-dom";
import LoginForm from "./components/LoginForm";
import styles from "./style.module.scss";

const Login = () => {
  const navigate = useNavigate();

  const navigateToRegistrationForm = () => navigate("/registration");

  return (
    <div className={styles.page}>
      <h1 className={styles.title}>Вход в систему</h1>
      <p className={styles.subtitle}>
        Выберите тип авторизации и заполните данные для входа в аккаунт
      </p>
      <LoginForm navigateToRegistrationForm={navigateToRegistrationForm} />
    </div>
  );
};

export default Login;
