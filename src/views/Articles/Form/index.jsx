import { useCallback, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Card from "../../../components/Card";
import "./style.scss";
import { Button } from "@mui/material";
import { useForm } from "react-hook-form";
import Firebase from "../../../FirebaseProvider/Firebase";
import { config } from "../../../FirebaseProvider/constants";
import ArticleForm from "./formComp";

const ArticleCreate = () => {
  const { id } = useParams();
  const { control, watch, handleSubmit, setValue } = useForm();
  const [description, setDescription] = useState();
  const navigate = useNavigate();
  const [firestore] = useState(() => new Firebase(config));

  function create(data) {
    console.log(data);

    firestore
      .createDocument(data, { collectionName: "articles" })
      .then((res) => {
        navigate("/articles");
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function update(data) {}
  const GetData = useCallback(() => {
    if (id) {
    }
  }, [id]);

  useEffect(() => {
    GetData();
  }, [GetData]);

  const onSubmit = (values) => {
    const data = {
      ...values,
    };
    console.log(data);
    if (id?.length) {
    } else {
      create(data);
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="mainContainer">
        <Card
          extra={
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <Button
                variant="contained"
                onClick={() => {
                  // navigate("/teachers/create");
                }}
                type="submit"
              >
                Yaratish
              </Button>
            </div>
          }
        >
          <ArticleForm
            control={control}
            description={description}
            setDescription={setDescription}
            watch={watch}
            setValue={setValue}
          />
        </Card>
      </div>
    </form>
  );
};

export default ArticleCreate;
