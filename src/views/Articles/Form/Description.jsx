import { useState } from "react";
import cls from "./createCustom.module.scss";
import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage";

export default function Description({ setValue, watch }) {
  const [file, setFile] = useState();

  const UploadFile = async (e) => {
    const file = e[0];
    const name = watch("Name");
    try {
      const storage = getStorage(); // Get Firebase Storage instance
      const storageRef = ref(storage, `articles/${name}`); // Reference to the destination path in Firebase Storage

      // Upload the image file to Firebase Storage
      await uploadBytes(storageRef, file);
      const imageUrl = await getDownloadURL(storageRef);
      console.log("Image URL:", imageUrl);
      setValue("imgUrl", imageUrl);
    } catch (error) {
      console.error("Error uploading image:", error);
    }
  };

  return (
    <div className={cls.uploadBox}>
      <input
        type="file"
        name="File"
        id="img"
        style={{ display: "none" }}
        accept="application/pdf"
        onChange={(e) => UploadFile(e.target.files)}
      />
      <label for="img">
        <div style={{ textAlign: "center" }}>
          {/* <CustomUploadIcon />  */}
          <div> PDF fayl yuklash </div>
          <div className={cls.upload_button}> Yuklash </div>
        </div>
        {file ? <div> {file} </div> : ""}
        <progress id="progress-bar" value="0" max="100"></progress>
      </label>
    </div>
  );
}
