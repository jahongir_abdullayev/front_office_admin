import { Grid } from "@mui/material";
import HFTextField from "../../../components/FormElements/HFTextField";
import Description from "./Description";
// import EditorComponent from "./Editor";

export default function ArticleForm({
  control,
  setDescription,
  watch,
  setValue,
}) {
  return (
    <div className="container">
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <HFTextField
            name="Mualliflar"
            control={control}
            placeholder="Mualliflar"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField name="Nomi" control={control} placeholder="Nomi" />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="Nashriyot"
            control={control}
            placeholder="Nashriyot"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField name="Nashrda" control={control} placeholder="Nashrda" />
        </Grid>
        <Grid item xs={6}>
          <HFTextField name="Betlari" control={control} placeholder="Betlari" />
        </Grid>
        <Grid item xs={6}>
          <HFTextField name="Yili" control={control} placeholder="Yili" />
        </Grid>
        <Grid item xs={6}>
          <HFTextField name="Shahri" control={control} placeholder="Shahri" />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="ProektShifri"
            control={control}
            placeholder="ProektShifri"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField
            name="MualliflarSoni"
            control={control}
            placeholder="MualliflarSoni"
          />
        </Grid>
        <Grid item xs={6}>
          <HFTextField name="link" control={control} placeholder="link" />
        </Grid>
        <Grid item xs={12}>
          <Description setValue={setValue} watch={watch} />
        </Grid>
      </Grid>
    </div>
  );
}
