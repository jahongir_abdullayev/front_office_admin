import { useCallback, useEffect, useState } from "react";
import "./Teachers.scss";
import Card from "../../components/Card";
import { useFirebase } from "../../FirebaseProvider/FirebaseProvider";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { DataGrid } from "@mui/x-data-grid";
import { writeFile } from "xlsx";
import { saveAs } from "file-saver";

const Articles = () => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const { getDocuments, deleteDocument } = useFirebase();
  const navigate = useNavigate();

  const GetData = useCallback(() => {
    getDocuments({
      collectionName: "articles",
      withDocumentId: true,
      filters: [],
    })
      .then((res) => {
        setData(res);
      })
      .catch((err) => {
        console.log("error", err);
      });
  }, [getDocuments]);

  useEffect(() => {
    GetData();
  }, [GetData]);

  function deleteArticle(id) {
    deleteDocument({ collectionName: "articles", id: id })
      .then((res) => {
        GetData();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const columns = [
    { field: "Yili", headerName: "O'quv yili", width: 100 },
    { field: "Mualliflar", headerName: "Mualliflar", width: 200 },
    {
      field: "Nomi",
      headerName: "Nomi",
      width: 200,
    },
    {
      field: "Nashriyot",
      headerName: "Nashriyot",
      description: "This column has a value getter and is not sortable.",
      sortable: false,
      valueGetter: (params) =>
        `${params.row.Mualliflar || ""} ${params.row.Nomi || ""}`,
      width: 200,
    },
    {
      field: "Nashr",
      headerName: "Nashr",
      width: 200,
    },
    {
      field: "Shahri",
      headerName: "Shahri",

      width: 200,
    },
    {
      field: "imgUrl",
      headerName: "imgUrl",
      valueGetter: (params) => `${params.row.imgUrl ? "Pdf yuklangan" : ""}`,
      width: 200,
    },
  ];
  console.log(data);
  return (
    <div>
      <div className="mainContainer">
        <div className="mt-4">
          <Card
            style={{ boxShadow: "0px 20px 20px rgba(135, 138, 146, 0.16)" }}
            footerStyle={{ borderTop: "1px solid #eee" }}
            extra={
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                <Button
                  variant="contained"
                  onClick={() => {
                    navigate("/articles/create");
                  }}
                >
                  Nashr qo'shish
                </Button>
              </div>
            }
          >
            <div>
              <DataGrid
                rows={data}
                columns={columns}
                initialState={{
                  pagination: {
                    paginationModel: { page: 0, pageSize: 10 },
                  },
                }}
                pageSizeOptions={[10, 20, 40]}
                checkboxSelection
                sx={{
                  width: "100%",
                }}
              />
            </div>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Articles;
