import { Incoming } from "./Incoming";

const MainPage = () => {
  return (
    <div className="container">
      <Incoming />
    </div>
  );
};

export default MainPage;
