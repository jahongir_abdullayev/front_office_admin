import { Grid } from "@mui/material";
import FormCard from "../../components/FormCard"
import { ColoredConfimIcon, ColoredDocumentIcon, ColoredRoundIcon, ColoredWalletIcon } from "../../utils/icons";
import style from './mainPage.module.scss'





export const Incoming = () => {

    const data = [
    {
        content : [
            {
                title : 'Choraklik reja',
                sum: '38 153 000',
                sumSuffix: "so'm",
                icon: ColoredDocumentIcon,
                percent: '',
                color: '#000000'
            },
            {
                title : 'Choraklik tushum',
                sum: '14 856 000',
                sumSuffix: "so'm",
                icon: ColoredWalletIcon,
                percent: '61,1%',
                color: '#4795EA'
            },
            {
                title : 'Qoldiq tushum',
                sum: '23 297 000',
                sumSuffix: "so'm",
                icon: ColoredRoundIcon,
                percent: '38,9%',
                color: '#DF2163'
            },
        ]
    },
    {
        content : [
            {
                title : 'Oylik reja',
                sum: '12 717 000',
                sumSuffix: "so'm",
                icon: ColoredDocumentIcon,
                percent: '',
                color: '#000000'
            },
            {
                title : 'Oylik tushum',
                sum: '6 300 000',
                sumSuffix: "so'm",
                icon: ColoredWalletIcon,
                percent: '50,5%',
                color: '#4795EA'
            },
            {
                title : 'Qoldiq tushum',
                sum: '6 417 000',
                sumSuffix: "so'm",
                icon: ColoredRoundIcon,
                percent: '49,5%',
                color: '#DF2163'
            },
        ]
    },
    {
        content : [
            {
                title : 'Kunlik tushum',
                sum: '425 000',
                sumSuffix: "so'm",
                icon: ColoredDocumentIcon,
                percent: '',
                color: '#000000'
            },
            {
                title : 'Kun davomida ko‘rsatilgan xizmatlar soni',
                sum: '12',
                sumSuffix: "ta",
                icon: ColoredConfimIcon,
                percent: '',
                color: '#4795EA',
                footerText: 'Oxirgi yangilangan vaqt   15:20'
            },
           
        ]
    },
    ]

    return (
      <Grid container spacing={4}>
        {data?.map((item) => (
          <Grid item xs={4}>
            <FormCard className={style.card} maxWidth={"100%"}>
              {item?.content?.map((elm) => (
                <>
                  <div className={style.cardItem}>
                    <elm.icon />
                    <div>
                      <div className={style.title}>{elm?.title}</div>
                      <div className={style.price} style={{ color: elm?.color }}>
                        {elm?.sum} {elm?.sumSuffix}
                      </div>
                      {/* <div> {elm.percent}</div> */}
                      <div className={style.footerText}> {elm?.footerText} </div>
                    </div>
                  </div>

                </>
              ))}
            </FormCard>
          </Grid>
        ))}
      </Grid>
    );
}