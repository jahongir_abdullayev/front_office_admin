import { useSelector } from "react-redux";
import { Navigate, Route, Routes } from "react-router-dom";
import { elements, pages } from "../hooks/useSidebarElements/elements";
import AuthLayout from "../layouts/AuthLayout";
import MainLayout from "../layouts/MainLayout";
import Login from "../views/Auth/Login";
import Registration from "../views/Auth/Registration";

const Router = () => {
  const isAuth = JSON.parse(localStorage.getItem("userInfo"))?.isAuth;

  if (!isAuth)
    return (
      <Routes>
        <Route path="/" element={<AuthLayout />}>
          <Route index element={<Navigate to="/login " />} />
          <Route path="login" element={<Login />} />
          <Route path="registration" element={<Registration />} />
          <Route path="*" element={<Navigate to="/login" />} />
        </Route>
        <Route path="*" element={<Navigate to="/login" />} />
      </Routes>
    );

  return (
    <Routes>
      <Route path="/" element={<MainLayout />}>
        <Route index element={<Navigate to="/teachers" />} />
        {elements?.map((item) => (
          <Route
            path={item.path}
            element={<item.component title={item?.title} />}
          />
        ))}

        {pages?.map((item) => (
          <Route
            path={item.path}
            element={<item.component title={item?.title} />}
          />
        ))}

        <Route path="*" element={<Navigate to="main" />} />
      </Route>
      <Route path="*" element={<Navigate to="main" />} />
    </Routes>
  );
};

export default Router;
