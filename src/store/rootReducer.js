import { combineReducers } from "redux";
import persistReducer from "redux-persist/es/persistReducer";
// import { alertReducer } from "./alert/alert.slice";
import { authReducer } from "./auth/auth.slice";
import { espReducer } from "./esp/esp.slice";
// import { measuresReducer } from "./mesure/measure.slice";
import storage from "redux-persist/lib/storage";
// import { actReducer } from "./docs/docs.slice";
import UserReducer from "./userInfo/user.slice";
// import { basketProductsReducer } from "./basketProducts/basket.slice";
// import { popupReducer } from "./popup/popup.slice";
// import { notificationReducer } from "./notificationShow/notification.slice";
// import { errorReducer } from "./error/error.slice";
// import { advertisementsReducer } from "./advertisements/advertisements.slice";
// import { settingReducer } from "./companysettingsStore/companySettings.slice";
// import { operatorReducer } from "./operator/operator.slice";
// import { rolePermissionReducer } from "./RolePermissions/relation.slice";
// import { tableReducer } from "./tableFilter/tableFilter.slice";
// import { docFilterReducer } from "./documentFilter/documentFilter.slice";

const authPersistConfig = {
  key: "auth",
  storage,
};

const userPersistConfig = {
  key: "user",
  storage,
};
// const docsPersistConfig = {
//   key: "docs",
//   storage,
// };

const espPersistConfig = {
  key: "esp",
  storage,
};

// const operatorPersistConfig = {
//   key: "operator",
//   storage,
// };

// const rolePersistConfig = {
//   key: "role",
//   storage,
// };

// const tablePersistConfig = {
//   key: "table",
//   storage,
// };

// const docFilterConfig = {
//   key: "docFilter",
//   storage,
// };

const rootReducer = combineReducers({
  esp: persistReducer(espPersistConfig, espReducer),
  auth: persistReducer(authPersistConfig, authReducer),
  // alert: alertReducer,
  // popup: popupReducer,
  // advertisements: advertisementsReducer,
  // measures: measuresReducer,
  // act: actReducer,
  userInfo: persistReducer(userPersistConfig, UserReducer),
  // basketProducts: basketProductsReducer,
  // notification: notificationReducer,
  // error: errorReducer,
  // docs: persistReducer(docsPersistConfig, actReducer),
  // settings: settingReducer,
  // operator: persistReducer(operatorPersistConfig, operatorReducer),
  // rolePermission: persistReducer(rolePersistConfig, rolePermissionReducer),
  // tableFilter: persistReducer(tablePersistConfig, tableReducer),
  // docFilter: persistReducer(docFilterConfig, docFilterReducer),
});

export default rootReducer;
