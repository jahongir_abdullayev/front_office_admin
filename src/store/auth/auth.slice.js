import { createSlice } from "@reduxjs/toolkit";
import { listToMap } from "../../utils/listToMap";

export const { actions: authActions, reducer: authReducer } = createSlice({
  name: "auth",
  initialState: {
    isAuth: false,
    uid: "",
    email: "",
    emailVerified: null,
    isAnonymous: null,
    providerData: [
      {
        providerId: "",
        uid: "",
        displayName: null,
        email: "",
        phoneNumber: null,
        photoURL: null,
      },
    ],
    stsTokenManager: {
      refreshToken: "",
      accessToken: "",
      expirationTime: null,
    },
    createdAt: "",
    lastLoginAt: "",
    apiKey: "",
    appName: "",
  },

  reducers: {
    setFirstTimeLogin: (state, action) => {
      state.firstTimeLogin = action.payload;
    },
    loginINN: (state, action) => {
      state.firstTimeLogin = true;
      state.isAuth = true;
      state.authData = action;
      state.token = action.payload.accessToken;
      state.organization = action.payload;
      state.branches = action.payload;
      state.permissions = listToMap(
        action.payload.permissions?.map((el) => ({
          ...el,
          name: el.name?.replace("/root/", ""),
        })),
        "name"
      );
      state.branchName = action.payload.branchName;
      state.branchNum = action.payload.branchNum;
    },
    login: (state, action) => {
      state.isAuth = false;
      state.uid = action.payload.uid;
      state.email = action.payload.email;
      state.emailVerified = action.payload.emailVerified;
      state.isAnonymous = action.payload.isAnonymous;
      state.providerData = action.payload.providerData;
      state.stsTokenManager = action.payload.stsTokenManager;
      state.createdAt = action.payload.createdAt;
      state.lastLoginAt = action.payload.lastLoginAt;
      state.apiKey = action.payload.apiKey;
      state.appName = action.payload.appName;
    },
    logout: (state) => {
      state.isAuth = false;
      state.uid = "";
      state.email = "";
      state.emailVerified = "";
      state.isAnonymous = "";
      state.providerData = "";
      state.stsTokenManager = "";
      state.createdAt = "";
      state.lastLoginAt = "";
      state.apiKey = "";
      state.appName = "";
    },
  },
});
