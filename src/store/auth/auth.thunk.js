import { createAsyncThunk } from "@reduxjs/toolkit";
import putLoginSign from "../../utils/putLoginSign";
import request from "../../utils/request";
import { espActions } from "../esp/esp.slice";
import { UserAction } from "../userInfo/user.thunk";
import { authActions } from "./auth.slice";

export const loginByToken = createAsyncThunk(
  "auth/loginByESPAction",
  async ({ tokenId, navigateTo, keyId }, { dispatch }) => {
    const pkcs7 = await putLoginSign(tokenId);
    let sign = await request
      .post("/login-id-card", { sign: pkcs7 })
      .then((res) => {
        dispatch(UserAction({ ...res.user }));
        dispatch(espActions.setSelectedToken(keyId));
        dispatch(authActions.loginINN(res));
      })
      .catch((err) => {
        console.log(err);
      });
    return sign;
  }
);
