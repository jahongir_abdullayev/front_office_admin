import { createSlice } from "@reduxjs/toolkit";

export const { actions: espActions, reducer: espReducer } = createSlice({
  name: "espKey",
  initialState: {
    selectedKey: null,
    keysList: [],
    key: {},
    token: [],
    keyObject: [],
    selectedToken: "",
  },
  reducers: {
    setSelectedToken(state, action) {
      state.selectedToken = action.payload;
    },
    setToken(state, { payload }) {
      state.token = payload;
    },
    setSelectedKey(state, action) {
      state.selectedKey = action.payload;
    },
    setKeys(state, { payload }) {
      state.keysList = payload;
    },
    setKey(state, { payload }) {
      state.key = payload;
    },
    espClean: (state) => {
      state.selectedToken = "";
      state.keysList = [];
      state.token = [];
      state.key = {};
      // state.keyObject = []
    },
    setKeyObject: (state, { payload }) => {
      state.keyObject = payload;
    },
    removeFromKeyObject: (state, action) => {
      const itemTin = action.payload;
      state.keyObject = state.keyObject.filter((item) => item.inn !== itemTin);
    },
  },
});
