const initalState = {};

const UserReducer = (state = initalState, action) => {
  const { payload } = action;
  switch (action.type) {
    case "USER_INFO":
      return {
        ...state,
        user: payload,
      };
    case "FACTURA_INFO":
      return {
        ...state,
        userHeaderInfo: payload,
      };
    default:
      return state;
  }
};

export default UserReducer;
