import reqGenerator from "../../utils/reqGenerator";

export const UserAction = (user) => {
  return {
    type: "USER_INFO",
    payload: user,
  };
};

export const getUserFacturaData = (inn, date) => {
  return function (dispatch) {
    return reqGenerator
      .get(`np1/bytin/factura?tinOrPinfl=${inn}&facturaDate=${date}`)
      .then((facturaData) => {
        const body = {
          tin: inn,
          companyName: facturaData.name,
          earning: "0",
          status: {
            title: facturaData.statusName,
            code: facturaData.statusCode,
          },
          taxGap: facturaData.taxGap,
          details: facturaData,
          taxPayer: facturaData.taxpayer,
        };
        dispatch({
          type: "FACTURA_INFO",
          payload: body,
        });
      });
  };
};
